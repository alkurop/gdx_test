package com.github.alkurop.game

/**
 * Created by alkurop on 8/1/16.
 */
object GameConfig {
    val CELL_X = 640f
    val CELL_Y = 480f
    val SCREEN_X = 640f
    val SCREEN_Y = 480f
    val CELL_COUNT_X = 6
    val CELL_COUNT_Y = 4
    val WORLD_X = CELL_X*CELL_COUNT_X
    val WORLD_Y = CELL_Y*CELL_COUNT_Y

}