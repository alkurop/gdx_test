package com.github.alkurop.game

import com.badlogic.gdx.ApplicationAdapter
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Camera
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.Sprite
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.input.GestureDetector
import com.badlogic.gdx.maps.objects.PolylineMapObject
import com.badlogic.gdx.maps.tiled.TiledMap
import com.badlogic.gdx.maps.tiled.TmxMapLoader
import com.badlogic.gdx.maps.tiled.objects.TiledMapTileMapObject
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer
import com.badlogic.gdx.math.*
import com.badlogic.gdx.utils.viewport.ScreenViewport
import java.util.*

/**
 * Created by alkurop on 8/1/16.
 */

open class KGdxGame : ApplicationAdapter() {
    val TAG = KGdxGame::class.java.simpleName
    lateinit var batch: SpriteBatch
    lateinit var mapSprite: Sprite

    lateinit var gameArray: ArrayList<ArrayList<Cell>>
    lateinit var cam: MyCamera
    lateinit var viewPort: ScreenViewport
    lateinit var gd: GestureDetector
    lateinit var map: TiledMap
    val unitScale = 1 / 16f
    lateinit var renderer: TextureMapObjectRenderer
    override fun create() {

        mapSprite = Sprite(Texture(Gdx.files.internal("cam.png")))
        mapSprite.setPosition(0f, 0f)
        mapSprite.setSize(GameConfig.WORLD_X, GameConfig.WORLD_Y)

        cam = MyCamera(GameConfig.SCREEN_X, GameConfig.SCREEN_Y)
        cam.zoom = 1.3f
        viewPort = ScreenViewport(cam)
        viewPort.apply()

        batch = SpriteBatch()
        createMapTable()
        initGestureDetector()
        Gdx.input.inputProcessor = gd
        map = TmxMapLoader().load("map.tmx")
        renderer = TextureMapObjectRenderer(map, unitScale)

    }

    private fun initGestureDetector(): GestureDetector {
        gd = GestureDetector(object : GestureDetector.GestureListener {
            override fun zoom(initialDistance: Float, distance: Float): Boolean {
                val zoomIn = initialDistance < distance
                Gdx.app.log(TAG, "zoom in $zoomIn")
                val newZoom = initialDistance / distance / cam.zoom * 50
                cam.zoom = newZoom
                Gdx.app.log(TAG, "zoom $initialDistance $distance  ${cam.zoom}")
                return true
            }

            override fun tap(x: Float, y: Float, count: Int, button: Int): Boolean {
                val vec3 = cam.unproject(Vector3(x, y, 0f))
                Gdx.app.log(TAG, "tap ${vec3.x} ${vec3.y}")
                map.layers[2].objects.forEach {
                    if (it is PolylineMapObject) {
                        val isInside = it.isInside(vec3.x, vec3.y)
                        if (isInside) {
                            Gdx.app.log(TAG, "tap is inside ${it.name}")
                            setTripletSelection(it.name)
                            return true
                        }
                    }
                }
                setTripletSelection(null)
                return true
            }


            override fun touchDown(x: Float, y: Float, pointer: Int, button: Int): Boolean {
                Gdx.app.log(TAG, "touchDown")
                return false
            }

            override fun fling(velocityX: Float, velocityY: Float, button: Int): Boolean {
                Gdx.app.log(TAG, "fling $velocityX $velocityY")
                val deltax = velocityX / 10
                val deltay = velocityY / 10
                if (Math.abs(deltax) > 1000 || Math.abs(deltay) > 1000)
                    cam.translate(-deltax / cam.zoom, deltay * cam.zoom)

                return true
            }

            override fun pan(x: Float, y: Float, deltaX: Float, deltaY: Float): Boolean {
                cam.translate(-deltaX * cam.zoom, deltaY * cam.zoom)
                return false
            }

            override fun pinchStop() {
                Gdx.app.log(TAG, "pinchStop")
            }

            override fun panStop(x: Float, y: Float, pointer: Int, button: Int): Boolean {
                Gdx.app.log(TAG, "panStop")
                return false
            }

            override fun longPress(x: Float, y: Float): Boolean {
                Gdx.app.log(TAG, "longPress")
                return false
            }

            override fun pinch(initialPointer1: Vector2?, initialPointer2: Vector2?, pointer1: Vector2?, pointer2: Vector2?): Boolean {
                Gdx.app.log(TAG, "pinch")
                return false
            }
        })
        return gd
    }

    private fun createMapTable() {
        gameArray = ArrayList<ArrayList<Cell>>()
        for (i in 0..GameConfig.CELL_COUNT_Y - 1) {
            val rowArray = ArrayList<Cell>()
            for (j in 0..GameConfig.CELL_COUNT_X - 1) {
                rowArray.add(Cell(
                          (j * GameConfig.CELL_X).toFloat(),
                          (i * GameConfig.CELL_Y).toFloat(),
                          Texture("${i + 1}.${j + 1}.png"),
                          Texture("u.png"),
                          false
                ))
            }
            gameArray.add(rowArray)
        }
        gameArray.reverse()
    }

    override fun render() {
        cam.zoom = MathUtils.clamp(cam.zoom, 0.00001f, GameConfig.WORLD_Y / cam.viewportHeight)
        cam.zoom = MathUtils.clamp(cam.zoom, 0.00001f, GameConfig.WORLD_X / cam.viewportWidth)
        cam.position.x = MathUtils.clamp(cam.position.x, cam.viewportWidth / 2 * cam.zoom, GameConfig.WORLD_X - cam
                  .viewportWidth / 2 * cam.zoom)
        cam.position.y = MathUtils.clamp(cam.position.y, cam.viewportHeight / 2 * cam.zoom, GameConfig.WORLD_Y - cam
                  .viewportHeight / 2 * cam.zoom)
        cam.update()
        batch.projectionMatrix = cam.combined
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)
        renderer.setView(cam)
        renderer.render(intArrayOf(0))
        batch.begin()
        gameArray.forEach { it.forEach { batch.draw(it) } }
        batch.end()
    }


    override fun dispose() {
        mapSprite.texture.dispose()
        batch.dispose()
        gameArray.forEach { it.forEach { it.dispose() } }
        renderer.dispose()
        map.dispose()
    }

    override fun resize(width: Int, height: Int) {
        super.resize(width, height)
        viewPort.update(width, height)
        cam.position.set(0f, 0f, 0f)
    }

    fun selectTriplet1(isSelected: Boolean) {
        gameArray[0][0].isSelected = isSelected
        gameArray[1][0].isSelected = isSelected
        gameArray[1][1].isSelected = isSelected
    }
    fun selectTriplet2(isSelected: Boolean) {
        gameArray[2][2].isSelected = isSelected
        gameArray[2][3].isSelected = isSelected
        gameArray[3][3].isSelected = isSelected
    }
    fun selectTriplet3(isSelected: Boolean) {
        gameArray[0][4].isSelected = isSelected
        gameArray[0][5].isSelected = isSelected
        gameArray[1][4].isSelected = isSelected
    }
    fun selectTriplet4(isSelected: Boolean) {
        gameArray[2][1].isSelected = isSelected
        gameArray[3][1].isSelected = isSelected
        gameArray[3][2].isSelected = isSelected
    }
    fun selectTriplet5(isSelected: Boolean) {
        gameArray[0][1].isSelected = isSelected
        gameArray[0][2].isSelected = isSelected
        gameArray[1][2].isSelected = isSelected
    }
    fun selectTriplet6(isSelected: Boolean) {
        gameArray[2][5].isSelected = isSelected
        gameArray[3][4].isSelected = isSelected
        gameArray[3][5].isSelected = isSelected
    }

    fun setTripletSelection(name:String?){
        when(name){
            "triplet1"->{
                selectTriplet1(true)
                selectTriplet2(false)
                selectTriplet3(false)
                selectTriplet4(false)
                selectTriplet5(false)
                selectTriplet6(false)
            }
            "triplet2"->{
                selectTriplet1(false)
                selectTriplet2(true)
                selectTriplet3(false)
                selectTriplet4(false)
                selectTriplet5(false)
                selectTriplet6(false)
            }
            "triplet3"->{
                selectTriplet1(false)
                selectTriplet2(false)
                selectTriplet3(true)
                selectTriplet4(false)
                selectTriplet5(false)
                selectTriplet6(false)
            }
            "triplet4"->{
                selectTriplet1(false)
                selectTriplet2(false)
                selectTriplet3(false)
                selectTriplet4(true)
                selectTriplet5(false)
                selectTriplet6(false)
            }
            "triplet5"->{
                selectTriplet1(false)
                selectTriplet2(false)
                selectTriplet3(false)
                selectTriplet4(false)
                selectTriplet5(true)
                selectTriplet6(false)
            }
            "triplet6"->{
                selectTriplet1(false)
                selectTriplet2(false)
                selectTriplet3(false)
                selectTriplet4(false)
                selectTriplet5(false)
                selectTriplet6(true)
            }
            else->{
                selectTriplet1(false)
                selectTriplet2(false)
                selectTriplet3(false)
                selectTriplet4(false)
                selectTriplet5(false)
                selectTriplet6(false)
            }
        }
    }
}
