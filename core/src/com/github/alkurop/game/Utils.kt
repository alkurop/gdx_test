package com.github.alkurop.game

import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.maps.objects.PolylineMapObject
import java.util.*

/**
 * Created by alkurop on 8/3/16.
 */
fun SpriteBatch.draw(cell: Cell) {
    this.draw(cell.getCurrentTexture(), cell.x, cell.y)
}

fun PolylineMapObject.isInside(x: Float, y: Float): Boolean {
    val name = this.name
    val xVert = this.polyline.transformedVertices.filterIndexed { i, fl -> i == 0 || i % 2 == 0 }
    val yVert = this.polyline.transformedVertices.filterIndexed { i, fl -> i % 2 != 0 }
    val points = ArrayList<Point>()
    xVert.mapIndexedTo(points) { i, f -> Point(f.toInt(), yVert[i].toInt()) }
    val hul1 = ConvexHullNew.convex_hull(points.toTypedArray())

    val xVert2 = xVert.plus(x)
    val yVert2 = yVert.plus(y)

    val points2 = ArrayList<Point>()
    xVert2.mapIndexedTo(points2) { i, f -> Point(f.toInt(), yVert2[i].toInt()) }
    val hul2 = ConvexHullNew.convex_hull(points2.toTypedArray())

    if (hul1.size != hul2.size)
        return false
    hul1.forEachIndexed { i, point ->
        if (point.x != hul2[i].x)
            return false
        if (point.y != hul2[i].y)
            return false
    }

    return true

}