package com.github.alkurop.game

import com.badlogic.gdx.graphics.Texture

/**
 * Created by alkurop on 8/1/16.
 */
val viewTable = 0

data class Cell(val x:Float, val y:Float, val texture:Texture,val selectedTexture:Texture, var isSelected:Boolean)
{
    fun dispose(){
        texture.dispose()
        selectedTexture.dispose()
    }
    fun getCurrentTexture():Texture{
        return if(isSelected)selectedTexture else texture
    }
}
