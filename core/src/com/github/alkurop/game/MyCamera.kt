package com.github.alkurop.game

import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.math.Vector3
import com.badlogic.gdx.math.collision.BoundingBox

/**
 * Created by alkurop on 8/3/16.
 */
class MyCamera : OrthographicCamera {
    @JvmOverloads constructor(viewportWidth: Float = 0f, viewportHeight: Float = 0f) : super(viewportWidth, viewportHeight)


    var left: BoundingBox? = null
    var right: BoundingBox? = null
    var top: BoundingBox? = null
    var bottom: BoundingBox? = null

    fun setWorldBounds(left: Float, bottom: Float, width: Float, height: Float) {
        val top = bottom + height
        val right = left + width

        this.left = BoundingBox(Vector3(left - 10f, 0f, 0f), Vector3(left - 10f, top, 0f))
        this.right = BoundingBox(Vector3(right + 1, 0f, 0f), Vector3(right + 10f, top, 0f))
        this.top = BoundingBox(Vector3(0f, top + 10f, 0f), Vector3(right, top + 10, 0f))
        this.bottom = BoundingBox(Vector3(0f, bottom - 10f, 0f), Vector3(right, bottom - 10, 0f))
    }

    var lastPosition = Vector3()
    override fun translate(x: Float, y: Float) {
        lastPosition.set(position.x, position.y, 0f)
        super.translate(x, y)
    }

    fun translateSafe(x: Float, y: Float) {
        translate(x, y)
        update()
        ensureBounds()
        update()
    }

    fun ensureBounds() {
        if (frustum.boundsInFrustum(left) || frustum.boundsInFrustum(right) || frustum.boundsInFrustum(top) || frustum.boundsInFrustum(bottom)) {
            position.set(lastPosition)
        }
    }
}