package com.github.alkurop.game;

/**
 * Created by alkurop on 8/4/16.
 */
public class Point implements Comparable<Point> {
    public Point() {
    }

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    int x, y;

    public int compareTo(Point p) {
        if (this.x == p.x) {
            return this.y - p.y;
        }
        else {
            return this.x - p.x;
        }
    }

    public String toString() {
        return "(" + x + "," + y + ")";
    }
}