package com.github.alkurop.game

import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.maps.MapObject
import com.badlogic.gdx.maps.objects.TextureMapObject
import com.badlogic.gdx.maps.tiled.TiledMap
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer

/**
 * Created by alkurop on 8/4/16.
 */
class TextureMapObjectRenderer : OrthogonalTiledMapRenderer {

    constructor(map: TiledMap) : super(map) {
    }

    constructor(map: TiledMap, batch: Batch) : super(map, batch) {
    }

    constructor(map: TiledMap, unitScale: Float) : super(map, unitScale) {
    }

    constructor(map: TiledMap, unitScale: Float, batch: Batch) : super(map, unitScale, batch) {
    }

    override fun renderObject(obj: MapObject) {
        if (obj is TextureMapObject) {

            batch.draw(
                      obj.textureRegion,
                      obj.x,
                      obj.y,
                      obj.originX,
                      obj.originY,
                      obj.textureRegion.regionWidth.toFloat(),
                      obj.textureRegion.regionHeight.toFloat(),
                      obj.scaleX,
                      obj.scaleY,
                      obj.rotation)
        }
    }
}

