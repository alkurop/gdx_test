package com.github.alkurop.game

import android.os.Bundle
import com.badlogic.gdx.backends.android.AndroidApplication
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration
import kotlinx.android.synthetic.main.main_activity.*

/**
 * Created by alkurop on 8/1/16.
 */
class MainActivityGdx : AndroidApplication() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val config = AndroidApplicationConfiguration()

        val gdxView = initializeForView(KGdxGame(), config)
        setContentView(R.layout.main_activity)
        container.addView(gdxView)
     }
}